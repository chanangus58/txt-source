「喔～歡迎回來～」

在辦公室裡處裡成堆文件的吉妮維亞抬頭歡迎著莉雅。
雖然臉上露出疲勞的表情，但她的雙眼正閃閃發光著。莉雅向女官打聽後得知她整晚都在組裝魔像。

「看來很辛苦呢」
「還好拉，只要埋首於自己的興趣中，就不覺得累了」

原來如此，的確有可能呢。
自己前世也曾經練習劍術到吐血的程度都不覺得苦。如今整頓處理法律等麻煩事時，所累積的都是精神上的疲勞。

「我有些話想說⋯⋯」
「是嗎，我也差不多想休息一下⋯⋯一起洗澡如何？」
「嗯，就這麼辦吧」

這兩人不光只是外表長的很像，興趣、思考模式也有很多相似的地方。例如喜歡洗澡就是一例。

兩人泡進了有著滿滿池水的浴池裡。
接著強硬的讓女官們退下，創造出專屬於兩人的空間。

如果是希茲娜身處這樣的環境，大概會覺得自己陷入了大危機之中吧。不過吉妮維亞很清楚自己並不是莉雅喜歡的類型。
吉妮維亞的頭髮有用毛巾包起來盤在頭上，而莉雅的頭髮就這樣泡在池水中。

「那，妳想談什麼？」
「是有關卡拉的事」

莉雅將卡拉在瑪拉達斯要塞的狀況說了出來。

「這樣阿，還真有那個孩子的風格」

看來這種情況對吉妮維亞而言並不稀奇。
雖然碩大的胸部在水面上載浮載沉，但她卻說出了極為關鍵的一句話。

「妳想要那個孩子嗎？」

莉雅倒吸了一口氣。
自己內心的想法就這樣被人直接說了出來。難道自己的臉上有露出那麼想要的表情嗎。

「如果妳是認真的話，不正面好好傳達妳的心情是不行的。那種旁敲側擊的手段是行不通的。但如果妳能認真的面對她⋯⋯」

女王的手指玩弄著莉雅的頭髮。

「那孩子也一定會認真回應妳的」
「可我是女的阿」

都到這個地步了，莉雅竟然還在提這件事，對此女王笑了起來。

「那又如何呢？」
「妳既然也身為女王應該很清楚吧，她身為貴族不能就這樣單單只成為我的女友阿」

面對著女王無言的回應，讓莉雅不由得一吐為快。

「必須有留下血脈的義務不是嗎？」

雖然說是取對方為妻，但只要對方有了對象就必須放手。莉雅一直以來都抱有這個覺悟。

「不對，雖然想法沒錯，但不對」

女王像謎一般的微笑著。而這個『我最了解卡拉了』的態度讓人不爽。

「哪裡不對了？」

莉雅的聲音大聲了起來。
嗯，必須承認。
對於眼前這個比自己還了解卡拉的女性，自己深深的感到忌妒。

「會這麼生氣，看來妳真的很喜歡那孩子呢⋯」

對於莉雅的威壓，女王只是聳聳肩的輕輕帶過。還真是深藏不露阿。
不過她隨即露出溫柔的笑容，以及略帶孤寂的感覺，讓莉雅沒辦法繼續說下去。

「就告訴妳那個孩子的秘密吧」

女王那金色的瞳孔朝莉雅投來強烈的視線，如果這邊避開頭的話一定會被輕蔑吧。

「那孩子沒辦法生產歐」

───

莉雅記不得之後發生的事情。
等回過神來便發現自己穿著浴衣頭髮也沒擦乾，就這樣呆坐在自己房間的床上。
為別人的事情感到如此震驚這還事第一次。

「她完全沒有月事，聽醫生說這大概是天生的」

對方似乎有說到這件事，但如今根本無關緊要。
所以不管莉雅是不是男的，
卡拉都不可能懷孕。
太痛苦了。
為什麼會這麼痛苦呢。當得知自己沒辦法懷孕的時候，很理所當然的就接受了。但同樣的狀況發生在卡拉身上時為什麼會⋯⋯

就算沒有小孩，人也是能夠好好活著。這是自己所擁有的覺悟，而且前世的自己就是這樣活過來的。
但是卡拉的情況不同。
她在得知自己沒辦法擁有後代子孫後，便決定為了其他人而活。
而吉妮維亞很清楚她的決心。
身為親友的她，很自然的被告知這些事情。但卻完全沒對我說。
也有可能是沒有一個好的時機來說明吧。
如果沒辦法生育的話，那麼卡拉就算一輩子都待在莉雅身旁也沒關係吧。
對於擁有竜之血脈的我們，到底能活過多少個年頭呢，這件事目前還是未知的。

───

「莉雅？」

傳來了敲門的聲音，聽聲音就知道門外是誰了。
所以沒有回應她。

「雖然聽人家講妳正在找我⋯」

卡拉很自然的進入房內，將手中的文件放在桌上後，走近穿著浴袍的莉雅。

「頭髮還是濕的阿」

卡拉取出了手帕擦拭著潮濕的頭髮，但手腕反而被對方握住。
突然站起來的莉雅，分別抓住了卡拉的雙手，就這樣將她推倒在床上。

「莉雅？」

即使身陷這種狀況，卡拉的眼神沒有一絲動搖。
是因為她有著將所有事情照單全收的強悍，還是說她其實是自暴自棄呢？

「我從吉妮維亞那聽說了⋯⋯妳沒辦法生育對吧」

自己也很清楚，如今說出來的話聲異常的冰冷。

「嗯，所以也沒有跟人結婚」

原來如此，猶如永遠的處女一樣。
雖然這嚴重的扭曲了她的人生，但對男人而言是非常理想的女神阿。
不但非常美麗，老化的速度也非常緩慢，此外那將近無限的愛可以公平的施捨給所有人。

水珠從莉雅那潮濕的頭髮上滲了出來，打濕了被推倒在床上的卡拉的臉。

「妳是我的妻子」

這一次不再有所顧慮。
莉雅親吻著卡拉。
一次、兩次、三次。舌頭在兩人之間來回穿梭。
對於那柔軟的舌頭，像似品味般輕輕觸碰著。而接吻的甘甜深入脊髓。
被壓在身體下方的卡拉，微微顫抖著。

充分的享受著餘韻的莉雅，離開了卡拉的雙唇。
但唾液在兩人之間牽起了一條銀線。

「總有一天一定⋯⋯」

莉雅低聲說著，這猶如誓言般的句子。

「總有一天一定要讓妳的身心全部都變成我的東西。我會將妳視為我生存在這世上的動力之一，也會將妳的一切都視為我自己的事情來應對」

說完了這像是詛咒般的言論後，莉雅放開了雙手。

而卡拉仍暫時就這樣躺在床上，過了不久爬起身後，稍微整理了一下自己的頭髮。
接著幫還坐在床上的莉雅整理了那凌亂的浴袍，而這期間，她的手指若有若無的觸碰著莉雅的肌膚。

這應該只是偶然吧，但莉雅卻對此感到微妙的快感。

「我，是為了國家、為了人民而活著」

卡拉告白著

「而如今，則是為了統領了那兩者的妳而活著」

雖然跟預期的不同，但這是卡拉的告白。

「總有一天，我一定會在妳的夢中出現」

對於這彷彿自暴自棄說出來的回應，卡拉朝莉雅行了個禮後便離開了房間。

───

莉雅並不知道。
卡拉被她推倒在床上時，心臟跳的有多麼激烈。
莉雅並不知道。
雙方在接吻時，卡拉也有品嘗著那甜美的滋味。
莉雅並不知道。
卡拉已經有多麼愛她了。

⋯莉雅不知道。

───

過了數天。
從前線傳來了莉雅在科爾多巴南方攻略並設置作為橋頭堡的要塞，正遭到科爾多巴大軍攻擊的消息。
莉雅僅將手頭上的奧加軍以及卡薩莉雅軍組織起來，就率領著他們作為援軍出發了。
而跟在她身後的毫無疑問是卡拉。

「敵方兵力共５個軍團，因為重視速度的關係，攻城器械以及補給部隊都只維持最低限度」

菲歐手上的報告，是來自於科爾多巴境內的獸人所提供的。
由於他們被納入科爾多巴領土的時間多半不到１０年，而且完全被當成比人類低下的種族對待著，讓他們全部都同意協助奧古斯大公國。
此外莉雅也承諾了讓各部族之長成為貴族。至於部族內部要怎麼處理這個貴族爵位，就不關莉雅的事了。

科爾多巴軍一得知我方援軍出動後，便馬上向後撤退了。
雖然不知道指揮官是誰，但這個判斷下得很正確。因為我方的兵力士對方的兩倍，此外其中有３萬人是奧加。
科爾多巴方依照截至目前為止的戰鬥結果來看，很清楚的知道跟這樣的軍隊正面作戰一定會輸。

「這樣一來有點麻煩阿」

莉雅嘀咕著，不過此時菲歐意外的提供了幫助。

「但是莉雅大人，科爾多巴的軍規有規定，一但與敵軍交會，除非情況特殊，否則沒有交戰就撤退是會受罰的」
「什麼！？」

莉雅不由得親自確認起文件來，隨即便露出笑容，那是嘲笑的笑容。

「科爾多巴出現制度疲勞了呢」
「制度疲勞？那是？」

雖然菲歐完全不懂這個詞彙的意義，但莉雅已經很明確感受到了。
仔細想想的話，科爾多巴軍的行動，都是很有規律的進行著，但是卻欠缺了類似一鼓作氣或壯大軍勢之類的舉動。
戰爭這種東西，是人們用鮮血彼此爭奪的過程。在戰爭中如果想要追求絶對的理性與秩序的話，那絶對是錯誤的。

對此，說不定能提早結束戰爭呢。
莉雅這樣思考著。
戰爭對人類而言毫無疑問是一種罪行，因此為政者必須採取最短路徑去終結戰爭。

「不光是獸人，也增加對人類的偵查。此外對於人類居住的領域也儘可能的散佈流言」

科爾多巴是個急速增加領土的國家，因此是不可能完全掌握住居住於其中的人民的心。
而且也有不少地方受到鎮壓而累積著不滿，如果讓原本的領主率軍前往的話，就有很大的機會可以掀起叛亂。

莉雅在這個時刻下定了決心，要儘早結束這場戰爭。