今天的入學考試和昨天的2個項目不同定在別的地方舉行。
好像是將平常是軍人們為了用來鍛鍊的某個專用的設施作為考場使用，在那裡面進行魔術的考試。

「啊，提奧君。這邊這邊～」

森精靈呼呼的揮著手。有相當多人數的應考生在。和昨天沒法比。在那之中莉茲的美麗也是出類拔萃的，格外引人注目。
她的手上不知什麼時候握了法杖。就是魔術師常持的東西。

「提奧君空手？」
「嗯，是啊……難道說需要什麼？」

「啊啊，這個？ 因為森精靈不擅長攻性術式啦。雖說如此但提奧君不知道嗎」
「不，確實有聽說過這種事呢。雖然很擅長回復系的術式，但到使用攻擊對手的術式為止需要在人類之上的驚人努力什麼的」

那好像是森精靈種族上的問題。雖然詳細的不太清楚，但有聽過因為她們崇拜的神是慈愛的神所以不適合攻擊之類的話。

「對對。考試無論如何也是攻性術式佔主要部分，所以森精靈可以拿能輔助力量的道具」
「雖說是不擅長，但這是被優待了呢」
「嗯。因為森精靈的魔力要比人類厲害呢。甚至回復術式什麼的比起鍛鍊了數十年的人類還是只用魔術的森精靈比較擅長。那樣的森精靈能用攻性術式的話當然會意想不到的強。所以在強化帝國的軍事力這樣的名目下，被允許使用這種東西哦」

「確實就算說是回復術式的考試也很難呢」
「沒有受傷的人就開始不了呢～……。嘛，比起那種樸素的東西還是能嘭嘭發射的攻性術式的考試比較愉快所以挺好的！」

森精靈是不是不喜歡爭鬥來著。或許我的森精靈觀相當過時了也說不定。
想著那種事時，接待小姐讓全員都能聽見地說道。

「欸，因為應考人數非常多所以考試分成複數回進行！ 第1回的考試已經截止了，請報考之後的考試的人盡快提出報考登記！」
「哦，不好不好。提奧君，快走吧。這樣下去天要黑了」

我跟著快步前往接待小姐身邊的她。然後，在那裡我和接待小姐的視線偶然相遇了。那個眼鏡是昨天的。
對方剛一看到我的身影就像斷線的木偶一樣當場倒下了於是又吵鬧了起來。

「啊呀，稍微往那裡讓讓，喂—喂？ ……啊，這是完全不能動彈了呢」

莉茲確認了接待小姐的情況後以輕浮的語氣說道。
……剛才我沒錯哦。只是對方隨便地害怕了。
一邊那樣辯解，一邊迅速向被替換的負責接待的人要了報考登記。
以和昨天同樣的要領流暢地寫了必要項目，正要提出的那時。

——嘭叭！！
和驚人的爆炸聲一起，堅硬的什麼粉碎四散的聲音回蕩在周圍。儘管是在遠處，卻感覺鼓膜嘩嘩震響了。

「嗚咕！？ 我的耳朵！！ 什麼什麼！？ 怎麼了！？」

對完全在預料之外的事態莉茲當場躺倒並捂著耳朵。森精靈特有的耳朵好好像適得其反。但是現在不是在意這種事的時候。
發出爆炸聲的是考場。我不顧周圍的人正困惑著，撥開人群到達了現場。

像要燒灼肌膚一樣的熱和猛烈的煙充滿了周圍。是誰用了魔術吧。這個我知道，但到底是誰。
煙消散後，理應在那裡的『考場其本身』被吹飛了。只有所剩無幾的瓦礫留下了遺跡。

「啊啊。搞壞了。名聞天下的米盧迪亞納領直屬軍校的魔術考試也就這種程度嗎。無聊」

在被破壞的建築物前的是1位少年。
烏黑的頭髮蓬亂，非常罕見的閃著銀色的瞳孔是其特徵。穿著法袍的那個身姿看起來大約才只有12歲。

(插圖002)

從那個小小的身體裡放出的莫大的魔力的質與量明顯壓倒了其他人。
容貌乍一看像是少女的少年露骨的嘆氣後說。

「喂，考官。這樣就行了吧？」

擔任魔術的考官的男性在發抖。

「不，不可能……！！ 這個考場可是被施放了能夠承受第八位階魔術的魔術結界啊！！ 那個，竟然……竟然」
「那麼，考試的題目要寫『禁止使用高於第八位階的魔術』啊。因為說即使你全力發射魔術也沒問題所以變成這樣了哦？」
「不可能……不可能」

「接受現實吧。就這樣虧你能當考官當到現在吶」
「這，這種事至今為止一次都沒發生過……！！」
「嘿欸。那麼，至今為止應考的人們全都是雜魚傻瓜嗎。看這樣子，帝國軍的魔術師們也好像沒什麼了不起的吶。無聊」

在遠處圍觀的應考生們由於這太過驚人的光景而大吃一驚。人聲嘈雜起來。

「吶，考官。這樣我就合格了吧？ 肯定是特待生了吶。太好了」

聳聳肩膀後說的少年的動作實在很優雅。要說第八位階以上的魔術的話，明明有名的魔術師全力解放之後即使魔力枯竭失去意識也不奇怪，這個少年卻在使用了那個後也好像沒怎麼樣。

確實厲害。雖然是很厲害不過。

「吶，提奧君……發生什麼了……痛痛……」
「啊啊，好像他把考試會場整個吹飛了哦。比起那個耳朵沒事吧？」
「還行吧。……嗯？ 是那孩子幹的？ 怎麼看都還是小孩啊」

不知是不是聽到了這個對話，少年將銀色的瞳孔轉向了這邊。

「喂，那裡的混蛋森精靈。因為外表小看人可是會遭殃的哦？ 也讓你嘗一發吧？」
「什！？ 等，哎，什麼啊這孩子！？ 嘴巴超損的！？ 話說你，滿足入學規定了嗎？ 最低也要15歲才能應考的？」

「就是因為這樣我才討厭憑外表判斷人的傢伙。我15」
「哎，騙人的吧！？」
「你這傢伙，想被整個烤嗎？」

少年的眼睛閃閃發光。相當認真吶，他。
這個動不動就打架讓我想起了昨天白色的那位，夏烏菈。

「嘛嘛，2個人都冷靜。話說……是你用了那個魔術嗎？ 好厲害呢」
「哈，並不厲害哦。只是帝國的水平低而已吧」

「但是，難辦了吶。這樣的話我們的考試就考不了」
「關我啥事。我能入學的話，會比你們全員都活躍哦。這樣就沒意見了吧」

聽到這句話的一部分應考生抱怨了，但被他一瞪後全員萎縮了。

「這就難辦了吧。我姑且也是希望入學的呢……對了，考官先生。能打擾一下嗎？」

我向吃驚得倒下了的考官大叔搭話。他一邊直流冷汗一邊沒說話注視我。

「考試會場壞了的話，考試已經考不了了嗎？」
「這，這個……沒有，前例」
「是嗎。但是考試的項目是這樣寫的呢。『諸位在那個分野顯示最大限度的實力』」

大概是不知道我想說什麼吧。考官只是點點頭。

「那麼，我『打倒他的話』能視為有入學的價值嗎？」

考官的臉完全呆住了的同時，從少年身上發出的露骨的殺氣刺痛了我的皮膚。

「……你這傢伙。看到那個慘狀，也還想說打倒我嗎？」
「當然」

我即答後，他多少顯出了怯陣的跡象。好像稍微警戒了。但是，馬上開了那張傲慢的口說道。

「有意思。試試吧。喏，隨時可以哦。用你能用的最大級的魔術打我試試」
「那樣你會死的所以不行呢」

「你！？ 說什麼，你這傢伙……！！」
「喂，那個提奧君！ 不要太刺激他比較好吧！？」

我不顧莉茲阻止繼續說。

「你，不是人類呢。雖然巧妙地化作了人類……但我覺得這是『龍』的感覺喲。怎麼樣？」
「龍！？」
「他說是龍！？ 是從瑟南龍王國來的嗎！？」

在後面變成觀眾的應考生們異口同聲地說。果然即使是在帝國也沒有看到龍的機會吧。而且外表看上去完全只是人類的少年。
被指出是龍的少年有些吃驚似的睜大了眼睛，但那之後以危險的語調說道。

「那又如何？ 話說在前面，入學簡章裡可沒寫禁止龍族哦」
「嗯。那種事無所謂。希望你趕快顯示真正的姿態。因為那樣的話」

我自然地翹起了嘴角。

「就算受到我的魔術，也就重傷就完了吧」
「……有意思」

爆發性的魔力包住少年，迸出眩目的光。
然後異常的威壓感在周圍漂浮了。這是真真正正的龍現出了身姿的證據。龍光是在那裡就會威壓周圍的人。就和我光是以本來的姿態呆著就會使周圍蒙受影響一樣。

在那裡的是體長大概有3米左右的閃著白銀色的龍。
從全身放出淡淡的光的那個身姿實在很神聖。是與被認為擁有極高智力的龍族相稱的姿態。

……好久沒看到龍了。是過了多少年呢。令人懷念的感覺啊。嘛，雖然眼前的他稍微有點小。
那麼，他會讓我多快樂呢？
