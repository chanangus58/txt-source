和瑪哈的約會很開心。

一邊在高級餐廳吃晚飯一邊思考那些事情。
當然，跟迪雅和塔爾特的約會也很開心，但是跟瑪哈的約會與她們稍微有點不同。

迪雅的情況是委身於我的安排，不斷地傳達自己的要求。
塔爾特的情況和迪雅一樣交給我。但是，在詢問的時候會細心照顧我，不要說出自己的要求。即使不開心，也會假裝開心的樣子。

而且，和瑪哈約會的時候，那個時候會根據不同的角色改變，為了積極地讓我開心，用頭腦來行動。

這樣既有刺激又有趣，又有新的發現，拓寬了自己的世界。
當然，我並不打算說兩個人的約會很無聊。迪雅的任性很可愛，能直截了當地提出要求，既易懂又輕鬆。
塔爾特的情況也是，被勉強裝作開心的部分，傳達了雖然很難做但是被仰慕的事，也可以很好地照顧。

也就是說，三個人有三種趣味。

「嗯⋯⋯今天的約會真好啊。⋯⋯正因為如此，現在必須得走了，真是可惜啊」
「我也很開心。接下來又是派對嗎？」
「是啊，是啊。是最後幾個無法拒絕的派對呢。我受夠了王都。閑暇的掌權者很多」

被歐露娜的魅力所吸引的貴族們，一旦歐露娜的代表代理來到了王都，就想邀請他們來參加自己的派對。
為了儘快確保商品，對歐露娜有興趣⋯⋯更重要的是，邀請了在貴族內成為話題的歐露娜副代表參加派對，是值得驕傲的。

「我早就在想，我已經幾乎不會再回到伊魯古了。⋯⋯想讓瑪哈成為代表而不是歐露娜的代表代理」

伊魯古・巴洛爾。
我的另一個名字，巴洛爾商會的少爺。

只是，使用那個身分的機會很少。
當然，使用那個名字的情況也很多。即使不化裝成伊魯古的樣子，使用那個名字的談判現在仍有很多在進行著。

「不要」

瑪哈馬上回答。

「即使現在也是實質代表吧。成為代表的話，工作會比現在更容易做」
「我知道。考慮到經營只有好處。果然，不管和什麼樣的人交涉，很多時候都感覺到代表和代表代理的頭銜完全不同的實感」

瑪哈的許可權，和作為代表的我完全一樣。
但是，對方卻不這麼認為。歐露娜的首位始終是伊魯古・巴洛爾，眼前的少女被認為是他的代用品。

「那麼，為什麼？你在顧慮什麼？」
「不是的。我喜歡作為羅格哥哥的手下。我想為你效勞，不管怎麼說我都不想放棄和你的關係。這是我的任性。我想永遠是你的瑪哈」
「真不像商人啊。如果是商人的話，擁有自己的店是夢想吧」
「⋯⋯也有那樣的夢。作為商人成長，積蓄金錢，開自己的店，總有一天會奪回父親的商會，這就是我的夢想」
「拿到歐露娜後，馬上就實現了」

瑪哈是我撿到的孤兒，原本是穆爾特在稍遠街道上的商會小姐。
瑪哈的性格和算計能力，作為商人的品味是在那裡磨練出來的。
她的父母被商會第二號謀殺，那名男子將孤獨的瑪哈收養為養子，奪取了商行之後，打算殺了她。
就在這時，瑪哈拼了命地逃到了穆爾特。

她認為現在的自己不可能挽回商會，首先想靠穆爾特來加強力量，召集街頭流浪兒們，讓孩子們開始做生意⋯⋯但是穆爾特開始致力於福利事業，在以補助金為目的的街頭兒童狩獵中被捕，和同伴們一起被丟進了環境惡劣的孤兒院，在那裡遇到了我。

她的夢想是找回父親的商會，救回街頭兒童時代的同伴，一起工作。

「不需要啊。就算沒有得到歐露娜，我也會實現的。與其這樣說，不如說幾乎實現著喲。報告書已經發送了吧。那個新人，大家都很活躍呢」
「原來是這樣啊」

調查了散落在孤兒院，以前夥伴們的詳細情況，然後去個別會面尋找。
那是一半感情，一半是實利。
雖說從小就在嚴酷的環境中生存著，也有人叫瑪哈領導人，但光靠孩子就能成功做生意的經驗也是寶貴的。

實際上，他們很活躍，在投資以上工作著。
歐露娜得到了難得的人才。

「而且，原來父親的商會也有三分之一的瓦解。收購計劃已經交給你了吧？」
「我正在看呢。」

以前瑪哈父親的商會，自從代表變了以後，業績惡化，開始賣掉資產。
瑪哈收購了出售的商店，開設了歐露娜的分店，以此為起點取得了相當大的業績。

我曾經對瑪哈說過『不要摻雜私情。但是，既然做了就要拿出成果』

她很聽我的話。

「我在你的身旁也能實現夢想。找回父親的商會，把以前的同伴也救了出來。而且，我會一直支持羅格哥哥的。我不會說其中一個。哪個都選。我是那麼優秀啊。所以，我能在你的身邊」

我微笑著。

瑪哈真的好強啊。
而且，對我的好意一直動搖著我的心。

「謝謝。瑪哈」
「不客氣。一開始，遇見我的是恩情。就那樣在那裡的話，什麼也做不了就被殺，或是被變態貴族賣了。多虧了羅格哥哥的幫助，也讓我得到了作為商人的力量。所以，你以為我不會報答你的恩情嗎」
「現在不是這樣嗎？」
「沒錯。現在也那樣想。不過呢，比起這個，我更喜歡羅格哥哥」

充滿了滿足、洋溢著幸福的表情。

心臟劇烈地跳動著。

我再次覺得瑪哈已經不是小孩子了，她已經變成漂亮的女人了。

「我也喜歡瑪哈哦」
「我知道。⋯⋯話雖如此，還是不想再重複了。按照今天的趨勢，應該能一直走到最後的。差不多該走了。羅格哥哥，給我獎勵和離別的吻吧」

瑪哈站起身來。

閉上眼睛，等待著我。
睫毛的長度，漂亮的皮膚，體味和低調的香水混雜了的氣味讓人非常在意。
就這樣，交換親吻。

離開嘴唇，瑪哈臉紅了按住嘴唇。

「⋯⋯我好高興。總是要求羅格哥哥接吻，明明都是臉頰和額頭」
「我覺得今天那邊比較好」
「呵呵，工作會加油的！」

瑪哈帶著歡快的聲音和笑容奔跑著出去。

⋯⋯那個瑪哈跑步，真是時間緊迫啊。

那個孩子幹得真好。
那麼，我先點香草來醒醒酒吧。
正享受著點好的茶，有人故意在眼前發出聲音坐下。

───

那裡有我的協助者。

「你真的很受歡迎啊。小天才魔法使，胸部大的槍使女僕，虛假的公主，其次是漂亮的能幹商人。大家都很可愛，又漂亮，又有才華，很迷戀你呢。」
「米娜啊。沒想到你在這座城市裡」

蛇魔族的米娜。毀壞昨天被殺的第二王子的罪魁禍首。
為什麼她在這裡，為什麼知道我在這裡？

不知從哪裡泄漏了情報⋯⋯需要調查。

「我裝作人類是為了享受人類的文化。不會不來慶典的。呵呵，非常開心！人類雖然渺小、弱小、醜陋，為什麼卻能創造出如此美好的東西呢？真的很可愛」

身為格蘭菲爾特伯爵夫人，潛入貴族社會中樞的只是一種娛樂，其規模與眾不同。

「你是為了閑聊才來這裡的嗎？」
「不會吧。呵呵，你幹得不錯啊。居然弄壞了我的玩具。那是第二個喜歡的」
「你在說什麼？」

誘餌。
我可沒蠢到留下暗殺證據的程度。

「哎呀，你真是裝蒜啊。」
「事實上，沒有關係。我是阿爾凡王國的忠誠貴族。應該不會對第二王子動刀吧」
「你是在拐彎抹角說『你有證據嗎？』，那麼，沒有那種東西啦。但是，我負責玩具的健康管理。那個玩具應該不會壞的。那麼，只能認為是被破壞了。而且，在那種情況下，能讓他『病死』的只有你。也就是說，是你殺的」
「這話太過分了。胡說八道」
「胡說八道嗎。但是，沒錯。⋯⋯我真的很生氣，想報仇啊。玩具被殺了，我也弄壞玩具吧？」
「那，可以當做是對我的宣戰宣告嗎？」
「是那邊吧，最先著手的是」

我和米娜靜靜地互相凝視。

雙方都沒有沒有露出一絲一毫的殺意。
正因為如此才不好辦。

所謂殺意，就是給對方情報。目標，時機，手牌等等。
慣於殺人的人泄露殺意只是在威脅的時候。

因此，習慣殺人的人在這種狀況下，完全沒有殺意並不是威脅，而是行動的前兆。

「啊，開玩笑了。我很喜歡那個玩具，但是你好像更有意思啊。因為這樣的事，失去你太愚蠢了。⋯⋯你也和我有同樣的想法吧。如果一個可愛的戀人被人殺害了，即使沒有證據，也會懷疑我，然後來殺我的，這一點是顯而易見的」

米娜聳聳肩
但是，我不會大意的。

還有，有充分的可能性。
在此基礎上，我們也進行了打探。

「重複一遍，我沒有殺人。況且，如果是那樣的話，先動手的就是你吧。我應該說過不要對朋友出手」
「哎呀，你知道嗎？現在最中意的事情。那孩子真好。可愛，可笑，醜陋，滑稽。所以才贈送的。⋯⋯啊，但是被擊中了痛處啊。我對你的朋友出手了。這樣的話，作為報復殺死戀人是不合理的。很好，這次的事情是彼此的」

懷疑變成了確信。
果然，諾伊斯的失蹤和米娜有關。
而且，從口吻來看，雖然不是平安無事，但並沒有死。

「⋯⋯到底諾伊斯怎麼了？」
「不久就會知道的。話說回來，麻煩的話到此為止吧？差不多該進入正題了」
「到此為止不都是正題嗎？」
「恩，無所謂。這樣的事」

所謂魔族是怎麼樣的？
我的最愛被殺了、說要殺了我的戀人、讓諾伊斯失蹤、做了什麼都無所謂。

「馬上，下一個魔族就要出現了。下一個魔族非常強大。那正是你和可愛的隨從們不能戰勝的程度。但是，請放心。因為援軍會來的」
「援軍？難道，你會幫忙嗎？不是想隱瞞對魔族的敵對嗎？」
「這才不會吧。敬請期待。話雖如此，你不是已經察覺到了嗎？」
「那麼，怎麼樣呢？」

說謊了。
這個故事的流向，誰會來，這一點是可以預料的。

「我把情報匯總到這裡了。啊，這個地方沒有看到。所有可以說的情報都在這裡嗎？我再也不想說什麼了。你很會觀察，一說話就容易泄漏」

米娜走了。

然後，瀏覽了資料。
不會吧，沒想到約會後就會變成這樣。

新魔族的出現是威脅。但是，這次有情報的話就能做得很好吧。