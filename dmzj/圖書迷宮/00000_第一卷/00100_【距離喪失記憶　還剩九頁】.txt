◇　　◇　　◇　【距離喪失記憶　還剩九頁】

「⋯⋯首先要對阿爾緹莉亞發動《滯時式速達信封》！」

你對魔導書蓋上封蠟，啟動其功能，得到翅膀的《信封》便在你的掌中振翅起飛，往書齋外飛去。
追著往收件人「魔王阿爾緹莉亞」的所在地飛翔的雙翼，你發揮加速的思緒和逐漸提升的體能，蹬著書齋的地面往迷宮衝了出去。
前往這個無限書庫的最深處，有魔王等待著的戰場。

（限時記憶漸漸流入我的腦中⋯⋯！被植入魔王人格的阿爾緹莉亞應該會吸迷宮生物的血，把它們變成一支不死的軍隊！把五感提升到極限⋯⋯！）

「──！這股屍臭⋯⋯是隸屬於魔王的不死軍嗎！」

失去了人性，接近最強吸血鬼的感覺器官偵測到飄散在空氣中的腐臭。隨後，一隻像熊一樣粗壯的手臂從書架後方朝著飛舞在前方的《信封》往下一揮。

「嘖！」

你伸出手指把《信封》彈開，穿越著閃過企圖撕裂你和《信封》的手臂。你一邊在迷宮的地面上往旁滑行一邊減速，感覺到形成複雜書庫的無數書架高牆以及包圍著你的黑暗中，有不死的怪物氣息正在蠢蠢欲動。

（⋯⋯我一頭衝進迷宮生物不死軍的側面了嗎？熊、獅子、猛禽，還有老鼠和蝙蝠⋯⋯！）

光是你的感覺能細數的部分，充滿在書架暗處的氣息就有數十只。
隸屬於魔王的不死軍縮短包圍的距離，然後一瞬間停止動作──

（來了！）

轟！魔獸如濁流般蠕動，從你的四面八方蜂擁而上。
可是雖然同為不死者，魔王粗制濫造的不死軍和沐浴了遠超過致死量的真祖之血，成為最強吸血鬼眷屬的你之間，身為不死者的等級完全不同。
你在緩緩流轉的視野中望向《速達信封》飛離的方向，瞄準一道道書架背後的一隻類似熊的不死者──

對其巨大身軀打出拳頭，以一記正拳攻擊對手！

「滾開。」

──一瞬間，承受了強大衝擊力的腐爛肉塊就像氣球一樣扭曲了形狀，發出砰的一聲巨響，化為一陣血肉和碎骨的雨中帶雪，混雜著周圍的所有物體飛了出去。

你沐浴在被衝擊波一舉擊碎的不死軍殘骸中，注視著《信封》的去向，帶著戰意說道：

「這條路⋯⋯是屬於我的！」

咚！你撼動了大地，往被拳頭打穿的不死軍中央衝刺。
你毆打、粉碎、毀滅了逼近過來阻止你靠近魔王的不死者〈僵屍〉，直奔幽暗的迷宮深處。

（⋯⋯不死軍的氣息愈來愈多了。我正在接近魔王⋯⋯！）

「不要⋯⋯妨礙我！」

你接連踩踏著地面和魔王的僕人，暴力地踩碎兩者，同時在圖書迷宮的第二樓層・四樓往黑暗的最深處不斷奔馳。

（就在前面，就在前面，就在前面⋯⋯！）

在你疾馳的方向，連一絲光線都不存在的漆黑之中，你終於捕捉到在虛空中振翅飛翔的《滯時式速達信封》

（阿爾緹莉亞就在前面等著我！）

朝著飛向魔王阿爾緹莉亞身邊的魔法雙翼，你穿越阻擋自己的無數書架，踩過被撕裂成碎片的不死者殘骸──

「阿爾緹莉亞啊啊！」

你衝進了升降機總站。

那裡是深深的血液之海。
在眾多不死屍骸深處，站在升降機前的魔王拖著一頭銀髮，在黑暗中回頭。
仿彿盛裝著血液的雙眸隔著流瀉而下的瀏海縫隙捕捉到你的身影。

「⋯⋯哼，是汝呀。」
「魔王⋯⋯！」
「礙事。妾身的眷屬，啃食以殺之。」

魔王已經對你毫無任何興趣，命令自己麾下擠滿了總站的不死者大軍殺死你。
面對同時瞪著你的幾百幾千只魔獸──失去死亡權利的亡者軍隊，你抽出事先放在板金鎧甲中的《收納書》

──現在，剛好抵達魔王正上方，就要打開的《滯時式速達信封》是對抗①不死軍隊的必殺一擊。

那個信封的裡面放著將近一千張的《折紙》

「《折紙》，變成裝水的木桶！」

在升降機總站的中央，飄散於空中的一張張《折紙》接收到你的命令，折疊成五十公升裝的木桶──

然後化為總計五萬公升的豪雨，在圖書迷宮中破裂。

「什⋯⋯麼⋯⋯！」

（變化前的《折紙》和裝水木桶的體積比大約是一百萬倍！大量的水瞬間膨脹，會變成衝擊波衝走①不死軍！）

在逼近到眼前的瀑布──對吸血鬼來說是劇毒的「水」面前，你一頭栽進排列在空中的十張《收納書》裡。
正如過去真祖所說的，你能夠被收納到旅行箱裡。既然能收納背包，當然也能收納旅行箱〈你〉了！

（一瞬間就好！只要能回避一瞬間的衝擊，我就能與魔王一對一戰鬥！）

你在多重化的《收納書》所形成的臨時避難空間中──

嘩啦！
聆聽魔導書所創造的「水」之激流逐漸破壞迷宮的聲音。

嘶嘶嘶嘶！收納著你的十張《收納書》被撕破了。離開《書》中的你站上衝擊波離去後的迷宮。
原本擠滿升降機總站的不死者軍團已經消失，在敵人被衝擊波粉碎並徹底清除的黑暗中，只有擁有理論裝甲的魔王還佇立在原地。

「⋯⋯大膽狂徒，竟敢將妾身召集之不死軍⋯⋯！」
「我消滅你的不死軍了，魔王。一對一決鬥才適合這個故事的結局。」
「⋯⋯呵呵呵，原來如此！『吸血鬼無法渡過流水』呀！看來汝似乎自認能阻止身為筆者的本魔王呢！」

沐浴在澆灌於緩慢世界的豪雨中，魔王以帶著瘋狂的笑容扭曲了臉。
被「聖堂」寫入的虛假記憶操弄，企圖以魔王身份復活的吸血鬼已經將你認知為「阻礙『聖堂』劇本的敵人」

接下來就是真正的戰鬥了──改寫故事結局的最終決戰即將開始。

「豈料偉大博士亦或吸血鬼真祖會於奧月綜嗣心中刻劃記憶，阻礙妾身成就霸業。然而抵抗也到此為止。汝終究不具英雄之器量吶！」

魔王露出真祖阿爾緹莉亞從未有過的嘲諷笑容，蔑視你的戰意。

「汝逃避過去的絕望，竄改了自身記憶，簡直是個與奧月戒相差十萬八千里的懦夫。懦弱得連心理創傷都無法克服的汝，要如何阻止本魔王？」
「⋯⋯」
「汝無法勝過妾身。失去魔法，失去記憶，失去父親，竄改了一切可信事物，且用盡記憶存量的汝，不可能打倒本魔王阿爾緹莉亞！」
「⋯⋯是啊。我的確不像父親一樣是當英雄的料。」

被降下的雨珠拍打著，你靜靜回答。

「我很無力，很脆弱，是個逃避真相的膽小鬼。我沒辦法成為像父親那樣的英雄⋯⋯可是就算如此，我還是有能相信的東西。」
「呵呵呵，承認顯得汝更加懦弱。汝這竄改自身記憶的窩囊廢如今又能相信什麼？汝之所有記憶或許都是經過竄改的虛構故事吶！」

唔哈哈哈哈哈哈！魔王高聲大笑的聲音在迷宮的黑暗中回響。可是你筆直地注視著帶有瘋狂氣息的深紅色眼瞳，如此宣言：

「我相信。不管被什麼樣的幻想背叛，我都相信阿爾緹莉亞。」
「⋯⋯唔嘻⋯⋯嘻哈哈⋯⋯啊哈哈哈哈！汝說相信真祖呀！嘻嘻嘻，汝可真悲哀！受記憶背叛，精神錯亂了嗎？啊哈哈哈哈哈哈哈！」
「⋯⋯」
「阿爾緹莉亞早已不復存在！真祖之記憶與人格已由本魔王將之改正！以筆墨塗改書頁，一張一張撕裂成碎紙，葬送於黑暗迷宮中！那虛假真祖的記憶已然從本魔王心中徹底清除！」

魔王就像是由衷感到可笑一樣，扭曲著表情嘲諷你的真祖。

「可惜呀奧月綜嗣！真祖阿爾緹莉亞已死於本魔王手下。」
「不對。」

你沐浴著不斷降下的雨水，帶著覺悟反駁。
就像是在仿彿靜止但確實逐漸落下的透明水滴中映照出阿爾緹莉亞與你的最後剎那。

「真祖沒有死。她沒有輸給你。阿爾緹莉亞就在這裡。」

你把手放在胸前，用指尖觸碰掛在脖子上的水晶護符。

「不靠語言或記憶，我的存在〈生命〉還記得。」

不管被什麼樣的悲劇侵襲，不管被什麼樣的記憶背叛，不管被什麼樣的命運撕裂；你都已經對水晶護符的魔力獻上了誓約。

「我已經發誓要相信阿爾緹莉亞了。」

你發誓賭上性命去相信拯救了你的吸血鬼。

「⋯⋯所以我要跟你戰鬥，魔王。」

剎那間的雨漸漸停止。
阻礙吸血鬼行動的「流水」開始從這個戰場的虛空中消逝。
你伸出右手臂，挽起制服的袖子。

「我不會讓你來決定這個故事的結局。」

你痛斥魔王，然後扯開自己右手臂的血管。從傷口噴出的血液借著艾莉卡的咒血能力瞬間凝結，形成一把石刀。

「賭上阿爾緹莉亞給我的一切，我要葬送魔王。」

你用再生後的右手舉刀，挑釁最強的魔王。
頁數已經用盡。《一千零一頁的最後祈禱》就結束在這一幕。身為魔導書的我已經無法再記錄戰鬥的情勢。
能夠決定往後結局的筆者，已經只剩下你和魔王了。
好了，捨棄《書》吧。
你已經不再需要《竄改記憶之書》

要為這部戀愛故事寫下結局的並不是《書》，而是讀者！

「接招吧，魔王。我就用一頁的篇幅解決你。」


◇　　◇　　◇　【距離喪失記憶　還剩零頁】

好了，開始最終決戰吧。

「要決定故事結局的──是我！」

我在簡短宣戰的同時，丟棄了《一千零一頁的最後祈禱》

我已經不需要用來逃避的記憶竄改。我要拯救阿爾緹莉亞。就算以這條命為代價，我也要從這片黑暗的最深處，將吸血鬼真祖阿爾緹莉亞帶回來！

「唔嘻嘻嘻嘻！如今故事已無法回頭！汝無法改變血腥慘劇之結局！世界將落入本魔王手中！」

我擺出跳躍的準備姿勢時，阿爾緹莉亞展開銀白色的翅膀咆哮。
這個升降機總站是我從「浮岳圖書館」來到中深層時經過的地方。這個升降機井會通往「藥草院」前幾大的人口密集地。如果被魔王阿爾緹莉亞進攻，那裡全部的人類都會化為吸血鬼⋯⋯！

「⋯⋯別想得逞！我不會讓阿爾緹莉亞傷害人類的！」

我蹬著迷宮的地面跳了起來，跳到被衝擊波破壞的升降機骨架上。
就算我沒有飛行能力，只要有體能和立足點，也能追上飛翔中的魔王！

「還想繼續追逐妾身嗎！那麼準備受死吧，奧月綜嗣！▓▓▓▓▓▓▓▓！」

我追著阿爾緹莉亞跳進直徑約五公尺的升降機井，尖銳的叫聲在其中回響。
這是魔王的能力之②，超高密度魔法言語的瞬間咏唱。
我和飛行的魔王大約相距十公尺。我沒辦法在咏唱結束前縮短這麼長的距離。而魔法一旦發行，我就會和這個空間一起被消滅，當場死亡。

「發行！」
「審問！」

可是我的手中還有真祖阿爾緹莉亞留給我的伏筆。
面對以壓倒性速度完成的咒語，我用力蹬著升降機往前進。

「超越四天之第五宇宙──」
「【你被『聖堂』竄改了記憶】！」

我發動《恒真審問書》──破壞咒語！

啪嘰咿咿咿！劇烈的摩擦聲響起。
魔王灌注到魔力回路裡的龐大魔力遭到攔阻，不完整的咒語開始失控──讓術者的神經系統瞬間沸騰！

「唔咿！嘎⋯⋯什⋯⋯什麼！將⋯⋯將《恒真審問書》用作反魔法！」

阿爾緹莉亞的表情因驚愕而扭曲。在魔法理論學的課堂上學到的知識奏效了。
使用魔法必須咏唱咒語。只要能阻礙咏唱，瞬間咏唱也沒有意義，走火的魔力會成為我破壞魔王的神經系統的武器！
如果對手是普通人，她應該能靠吸取血憶得來的再生能力消除這個破綻──

「唔，翅膀不聽使喚⋯⋯！」

但在極度接近最強〈阿爾緹莉亞〉的眷屬〈我〉面前，就連剎那也像是永遠的靜止！

「太慢了！」

我踢著升降機的鋼筋，把跳躍的動能集中到右手，用劍尖使勁揮砍。
刺中理論障壁的刀刃雖然被粉碎，卻還能破壞大量的魔法陣！

「唔哦哦哦啊啊啊！」
「唔──喝！」

斷裂的刀鋒正要貫穿阿爾緹莉亞的瞬間，尖銳的手刀從左側逼近我。
在這種狹窄的升降機井裡，回避是不可能的，現在只能攻擊！
我揮舞右臂，打碎阿爾緹莉亞的下頷。
同時，我的頸部被應聲打飛。

「嘎呼！」
「嘎⋯⋯啊！」

最強吸血鬼的不死特性讓我的腦瞬間復原。
因為我的攻擊粉碎了魔王的頷骨，所以在完全再生之前，她都無法開始咏唱。問題是我的身體從鋼筋上被彈開，拋飛到升降機井的空中了！

「呵哈⋯⋯啊哈哈哈哈！使用《恒真審問書》妨礙咏唱，再以血液石化能力制造血刃呀！真祖所準備的伏筆可真有意思！⋯⋯不過汝，妾身的傷勢可立即治癒，汝可有辦法耐得住生成武器時的出血？」
「唔⋯⋯還沒完！」

即使運動能力和思考能力加速了，重力加速度也不會有變化。不像魔王一樣有翅膀的我一旦從鋼筋上失足，就會頭下腳上地直墜幾公里下的總站。

《審問書》的頁數也所剩不多了。變異性休克離我愈來愈近。在魔王再次使用瞬間咏唱之前，我必須盡早打倒她，搶到《吸血奇譚》才行⋯⋯！

「以《審問書》阻礙咏唱也僅止於頁數用盡之前！《恒真審問書》還剩下幾頁呀？妾身可要加快咏唱速度了！▓▓▓！」

魔王開始咏唱的同時，我在空中飛舞的身體終於踩到升降機的鋼筋。雖然遠離了魔王，但這樣的距離反而更容易行動！

「審問！【你知道《圖書迷宮的吸血奇譚》的啟動碼】！」

──如果要趁技後僵直的空檔進攻的話！

「發行・驅獄炎之天Rz999f！」

發動的《審問書》阻礙了阿爾緹莉亞的瞬間咏唱。我衝進魔王懷裡，使出全力以刀刃攻擊這一瞬間的破綻。

「喝啊啊啊啊啊！」

啪嘰咿咿！現場爆出一陣干擾音。打破了幾十道障壁，密度比想像中更低！

「對抗③理論裝甲的策略⋯⋯我要用咏唱阻礙和物理攻擊把你的障壁全部打穿！」
「唔哈哈，辦得到就儘管試吧！朱染七天，來者攜炎！」

一陣瘋狂大笑後，魔王終於開始使用普通魔法言語咏唱。雖然和瞬間咏唱不同，威力會大幅減弱，卻能讓《審問書》發動的時機困難得不切實際。
我能採取的手段只剩下一種，那就是以近身戰打斷她的咏唱！

「破壞與再生之證，淨化之炎可毀天！」
「喝啊啊！」

我用一次跳躍縮短距離，使出橫向揮砍。從在陽台防御黑樁的描述就看得出來，障壁和魔王本身有一段距離。只要逼近到一定程度，就能夠以接近最強吸血鬼的我的臂力斬開多重障壁！

「唔哈哈哈！宣告末日寒冬來臨之──」

魔王不理會碎散的理論障壁，繼續咏唱，而我用盡全力揮砍！

「灼⋯⋯啪噗！」

正要念完咒語的舌頭沒能碰到本該存在於原處的口蓋，只能舔拭空氣。我的攻擊刺中頭蓋骨，切斷了上頷，破壞阿爾緹莉亞的咏唱和腦髓。

「⋯⋯唔哈！啊哈哈⋯⋯啊哈哈哈哈啊哈哈哈哈哈哈哈哈！」

可是最強吸血鬼不可能只因為失去腦部就死亡。如果不在抵達「圖書館」之前阻止魔王，這個故事就會迎向最糟的結局！

「唔嘻嘻嘻嘻嘻嘻嘻！肉搏戰雖有趣，但汝若不奪走《圖書迷宮的吸血奇譚》，便無法消滅本魔王！看吶，升降機井已來到盡頭！」
「唔⋯⋯距離『浮岳』的住宅區還有一段距離！」

阿爾緹莉亞飛翔的目標──我們頭頂上的黑暗中出現了像是針孔般的光點。糟糕了。要是「浮岳」的居民就這麼被她變成吸血鬼，我就沒有勝算了！

「好了奧月綜嗣！本魔王吸吮人血之前，汝可有辦法戰勝此劇本？」
「我不就說了⋯⋯我一定會贏！」

伴隨著怒吼所放出的攻擊在阿爾緹莉亞和我的正中央發生激烈衝突。
壓倒性的動能交換粉碎了一把刀和一雙手臂，使之轉換成大量的肉屑。

「啊哈哈哈哈哈哈哈！怎麼了！就這點程度豈能殺死本魔王！」
「喝啊啊啊啊！」

既然雙方都是不死者，這場戰鬥就不可能因肉體的死亡而分出勝負。
吸取力量、增幅、加速，然後直到某一方用吸血牙刺入對手的頸部前──

這場死鬥都不會結束。

「嘻哈哈哈哈！汝不可能獲勝！不可能阻止此劇本！」
「唔⋯⋯我明明拿刀，為什麼就是壓制不住！」

我們一邊將彼此的肉體轉變成血沫，一邊互相爭奪。
攻擊的質量明明是我佔上風，卻因為吸血鬼的純粹能力差距而僵持不下。
魔王能夠不斷變得更強。借由吸取我的血憶──就連在這場戰鬥的期間也是！

「嘖──喝啊啊啊啊！」

我在空中砍掉朝我進攻的手刀，勉強用被這股力道打碎的刀刺向對手。
情況很不妙。原本存在於魔王和我之間的間隔差距被血憶的吸取逐漸顛覆⋯⋯！

（這樣下去是贏不了的⋯⋯既然如此！）

我用刺出的裸露掌骨貫穿阿爾緹莉亞的肩頭，使魔王的左臂停止動作。我揮起右手，對毫無防備的身體⋯⋯

「這步棋──失算了吶！」

我打出的拳頭被魔王的手掌接住，往上大幅擊飛她的身體！

「糟糕⋯⋯！」

魔王的肉體被彈飛並粉碎，卻又借由壓倒性的不死特性而瞬間再生。靠著毆打所產生的作用力與反作用力，她的身體朝著「浮岳圖書館」加速前進！

「慢⋯⋯慢著，我不能讓你過去！」
「唔哈哈哈，妾身感覺得到朝日灼燒肌膚的觸感！終於抵達『圖書館』了！」

我再怎麼拚命踩踏升降機的骨架，還是追不上飛行中的魔王。
上方綻放的光芒在轉眼間擴大──

「浮岳圖書館」的景色在我們眼前展開。

「──啊哈哈哈！妾身已等候多時呀，人類！首先就吸盡此『圖書館』的居民之血，作為妾身稱霸世界之糧吧！」
「唔⋯⋯還沒結束！距離浮島上的住宅區還有一公里以上！」

升降機井所連接的前端是「浮岳圖書館」的底部，除了鋼筋之外空無一物。要抵達浮岳的人口密集地，還需要攀爬一公里以上的距離⋯⋯！

「天真吶奧月綜嗣！本魔王豈會放任汝繼續攀登！只要破壞此升降機井，汝可就再也到不了浮岳啦！」
「！」

尖銳的吶喊敲響鼓膜的同時，一陣衝擊伴隨著轟隆巨響在腳下流竄。魔王的強勁臂力開始破壞我必須攀爬的升降機，把鋼筋扯得面目全非。
我腳下的立足點失去支撐，逐漸傾向什麼都沒有的半空中！

「啊哈哈哈哈哈哈！好了，墜落身亡吧，奧月綜嗣！」
「唔⋯⋯我不能掉下去！」

被重力吸引的我減緩上升的速度，抽出夾在肩甲裡的一張《收納書》⋯⋯沒問題。真祖為我刻劃的伏筆不會因為這點程度的事就敗下陣來！

「我不會死的！在擊敗魔王的故事之前都不會死！」

我撕破了編入伏筆的《收納書》──

雙腳噠的一聲在黎明的虛空中出現的長著翅膀的冰層上著地！

「什⋯⋯什麼！」
「你以為真祖連這種程度的狀況都沒有考慮過嗎？魔王！」

從《收納書》的書頁中出現的是以艾莉卡的咒血能力石化的冰層岩盤。直徑兩公尺的圓盤邊緣用《折紙》製造出來的繩子連接著寄送給奧月綜嗣的無數《信封》

朝我的方向振翅飛來的《信封》翅膀沒辦法送達，而是推擠大量的空氣，承載著圓盤和我的重量飛行！

「豈⋯⋯豈有此理！此⋯⋯此戰略根本不存在於妾身所吸取之記憶⋯⋯！」
「唔⋯⋯哦哦哦哦哦！」

我朝著阿爾緹莉亞，用盡全力踩踏飄浮在空中的冰層。被石化的咒血凍結得極度堅硬的魔法冰塊用強烈的加速度把我的肉體往反方向推。

「我要把你的理論障壁全部打破！」

瞬間形成的血液之刃在黎明的微光中華麗地一閃，將魔王的障壁一刀兩斷。還差一點。還差一點就可以貫穿保護《吸血奇譚》的理論裝甲了！

「唔，於虛空中製造戰場不過是小聰明！妾身這就打碎冰層！」
「太天真了！」

阿爾緹莉亞煩躁地大叫，作勢對冰之岩盤使出強勁的回旋踢。可是我在她的腳跟正要踢中冰層的那個剎那將一部分的水解除石化。
變回液體的水輕巧地躲過了踢擊──咬住魔王的腳跟後再度石化！

「什⋯⋯什麼！」
「抓到你了！」

左腳被拘束的魔王驚愕地倒抽一口氣。
障壁單薄的極近距離。瞬間咏唱和普通咏唱魔法都封住了。能夠讓魔王吸取血憶或變成吸血鬼的人類和迷宮生物也不存在於這個「浮岳圖書館」的空中。
接下來只要我的意念和自我吸血能超越魔王的體能──就贏得了！

「唔哦哦哦哦！」

我在雙手中形成血液之刃，開始刨挖如曼陀羅般複雜的層層理論裝甲。腳被束縛住的魔王抵抗著，而我砍了又砍，砍了又砍，斬開她的障壁。

「可恨呀啊啊啊！」
「砍碎！」

以斜十字形揮下的刀鋒犧牲了刀身，斬下阿爾緹莉亞的雙手。
直到肉體再生為止的一剎那，魔王都不可能再反擊了！

「我就用！」

我丟棄粉碎的雙刀，放低重心，擺好架式。我接著以【格闘技的精髓】收縮全身的肌肉組織，用盡全力打出緊握的右拳。

「這一擊！」

拳頭打中魔王的障壁，直接對準心窩的中央。
吸血鬼化的肉體所使出的全力一擊壓碎了最強吸血鬼的胸腔後力道仍然不減，順勢撕裂了魔王的左腳，將她高高打向天空。
我從勉強保留下來的右肩鎧甲中再抽出《收納書》，咬破它。
為了擊發藏在裡面的用我的血凝固的冰槍。

「消滅魔王！」

從《收納書》裡射出的冰槍就像箭矢一樣朝魔王飛去──

咚的一聲！
把吸血鬼真祖的肉體釘在浮岳的底部！

「⋯⋯唔呼⋯⋯呼⋯⋯呼！⋯⋯我贏了，魔王！」

我等待肉體的再生，在一陣蒙蒙的沙塵中喊道。
流過「浮岳圖書館」的風吹散了打碎岩石時揚起的粉塵──在浮島的峭壁側面，魔王就像展翅的昆虫一樣，被逐漸融化的冰槍釘在上面。

「嘎噗⋯⋯！豈⋯⋯豈有此理⋯⋯本魔王⋯⋯竟被逼到⋯⋯如此絕境⋯⋯！」
「⋯⋯魔王，貫穿你的冰牙之槍會慢慢解除石化。無法渡過流水的吸血鬼是逃不過冰槍的束縛的！」

這麼一來就封住了魔王的行動。不死者軍隊、瞬間咏唱、理論障壁、血憶吸取能力，魔王的所有戰力都被我破解了。
我確信自己獲得了這場戰鬥的勝利，這麼宣告：

「那麼，我要奪走《圖書迷宮的吸血奇譚》了。」

我把手伸進制服的懷中，捏住一個純白的信封，裡頭封著我的王牌──複寫了「破刃暴風槍」的《複寫紙》

只要用這招武裝解除魔法直接擊中魔王，奪回《圖書迷宮的吸血奇譚》，我就能從竄改過的記憶中救出阿爾緹莉亞。

「我要竄改魔王的劇本！」

我抽出信封，抽出從我的記憶中撕下的《最後祈禱》！

〈──那就是真祖阿爾緹莉亞刻於汝心之記憶吧？〉

我聽見了超音波構成的低語。

「！」

正要破曉的黑夜，即將灑落在「浮岳圖書館」的朝陽蒙上了陰影。簡直像是時間倒轉了似的，應該就快結束的薄暮變得更加昏暗。

〈戰勝本魔王？少放肆了，半人半鬼。〉

不可聽音域的聲音震撼整片大氣，本能感覺到的恐懼讓我的全身都起了雞皮疙瘩。

〈本魔王阿爾緹莉亞・阿爾・阿塔納西亞・安納西亞・奧莎納西亞乃迷宮最強吸血種，被譽為吸血鬼真祖之能力吞噬者，不死之王。〉

〈──吾名為「圖書迷宮的銀夜」。〉

轟！

阿爾緹莉亞大喊的同時，她的肉體爆出一大群蝙蝠，變化成一陣銀色暴風。爆發性增殖的翼膜蓋過了朝日，在「圖書館」內製造出夜色⋯⋯！

「銀⋯⋯『銀夜』⋯⋯！」

〈唔哈哈哈哈哈！本魔王的血憶吸取能力將從「夜」之中的所有生者身上奪走血憶！自抵達此「圖書館」的那一刻起，就注定由妾身獲勝了吶！〉

化身為「夜」的最強吸血鬼瘋狂大笑，震撼了整片大氣。
銀白色蝙蝠舖天蓋地，其翼製造出漆黑暗夜。
這是迷宮最強的吸血種，吸血鬼真祖的絕對領域。
阿爾緹莉亞的異名由來，「圖書迷宮的銀夜」──逐漸覆蓋「浮岳圖書館」！

〈所幸妾身為求謹慎，試著佯裝落敗。汝腦中竟埋有本魔王未知之記憶，奧月綜嗣！〉

「嘎⋯⋯呼⋯⋯咿⋯⋯！你⋯⋯騙⋯⋯！」

〈誠然。吾身乃「圖書迷宮的銀夜」。姑且不論最弱狀態時，在吸取了血液與血憶之本魔王面前，肉體拘束毫無意義！〉

（唔⋯⋯呼⋯⋯呼吸困難⋯⋯！我的體能被魔王吸走了嗎⋯⋯？）

我感覺到缺氧般的窒息感和暈眩，身體虛弱地跪下。
就連繼承了真祖的不死之身，有能力抵抗「銀夜」的我都因被吸取血憶而意識不清。如果這種濃度的「夜」抵達浮岳的都市區，不曉得會造成以幾千人為單位的傷害⋯⋯！

「魔王⋯⋯你⋯⋯這個⋯⋯！」

〈呵呵呵呵，與亞人為伍的人類有幾兆死傷都不足惜！好了，奧月綜嗣，是時候開始反擊了吶！〉

「唔⋯⋯繞到我的背──」

我感覺到魔　　氣息出現　　　　瞬間，　　就被
王的　　　　在背後的　　　意識　　壓垮了。

「──嘎啊！」

最強吸血鬼的不死特性讓我的意識再生的瞬間，我差點被血液嗆到，吐出一口鮮血。
或許是聽覺遭到破壞，我有嚴重的耳鳴，沒辦法掌握身體的平衡。強烈的噁心感讓我忍不住想按住嘴巴，這才看到四肢除了右臂之外都被扯斷了。

「唔⋯⋯啊⋯⋯咿⋯⋯！」

緊接在視覺後出現的痛覺讓我知道全身各處都有致命性的毀損。
心臟失去應該輸送的血液，被壓扁了。我之所以不能順利呼吸，似乎是因為肺部被某種東西衝撞導致破裂的關係。
不過一次攻擊──我就被打成瀕死的重傷。

〈⋯⋯呵呵呵呵。原以為會粉碎為細屑，永遠不再復甦呢，汝可真幸運。若非被雜貨島之鐘塔阻擋，汝可就要消失在「圖書館」的遠方了。〉

操控「銀夜」的最強吸血鬼在銀白色的模糊視野中現出身形。

⋯⋯看來我似乎是受到「銀夜〈阿爾緹莉亞〉」本體的攻擊，從浮岳的底部貫穿了岩盤，直接飛越空中，撞上了雜貨島的鐘塔。

「咕⋯⋯唔唔唔唔⋯⋯！」

視神經連接左眼球和大腦，雙眼開始測量我與魔王的距離。
好近。逃不掉了。骨骼的再生還要花費幾秒？我的記憶能維持到那個時候嗎？

──不，一定無法維持。

「可⋯⋯惡啊啊⋯⋯！」

我勉強活動差點被扯斷的右臂，把手插進制服的內口袋。
為了抓住從魔王手中奪取《吸血奇譚》的最後王牌──複寫了武裝解除咒語「破刃暴風槍」的咒紋的《鏡像複寫紙》

可是我所要找的紙張不在那裡。

〈這是什麼呀？〉

因為封印我的記憶的信函──

已經被魔王的指尖奪走了。

──我感到不寒而慄。

在確定落敗的瞬間，如死亡永眠般冰冷的恐懼流竄我的全身。

「啊⋯⋯嗚⋯⋯黑⋯⋯黑暗⋯⋯！」

我對這種感覺有印象。
他人的記憶流入時所產生的排斥症狀，變異性休克的前兆。視野邊緣出現黑色的扭曲，就像是被黑暗侵蝕精神一般，我的靈魂即將遭到吞噬。

〈呀哈哈哈哈！被奪去真祖之記憶〈書頁〉，大量失血而引發變異性休克啦。如此一來汝之劇本也到此為止了吶！〉

已經無法再生的聽覺勉強能聽到震撼大氣的聲音。
意識逐漸遠去。
視野逐漸模糊。
我已經無法理解任何語言。
我能感覺到全身的神經正在急速衰退。

〈汝將死去。被記憶回溯吞噬並陷入意識斷絕，所有記憶遭到破壞直至死亡。名為奧月綜嗣之角色將從這世上永遠消失。〉

高速流入的龐大記憶將我的人格塗改得面目全非。
魔王的聲音在轉眼間遠去，意識往腦髓的某個深處逐漸墜落。
黑暗逼近我。死亡逼近我。世界的毀滅逼近我。

〈好了，妾身這就當面撕毀汝所倚靠之最後希望吧♪〉

魔王的雙手手指放在我的書頁上。
我不能輸。我不能逃。我非戰鬥不可。
因為我在五年前，應該有從記憶回溯的黑暗背後獲得某種東西。
在這場深沉絕望之夜的另一頭，我應該有得知些什麼。

〈看吶，妾身要撕破嘍，要撕破嘍！〉

因為我的記憶被阿爾緹莉亞竄改過。

〈呵呵呵，嘶嘶嘶嘶⋯⋯嘶嘶！〉

因為我的記憶裡──

〈啊哈哈哈！實在遺憾吶奧月綜嗣！如此一來魔王的故事便完結──〉

刻劃著改寫這個故事結局的真相。

──滋哩，一陣肌肉撕裂的聲音響起。

石化黑樁從開封的《收納書》中射出，貫穿了魔王的肉體。

〈嗄？發⋯⋯生什麼⋯⋯咕噗呼！〉

「⋯⋯很可惜，魔王。」

六張紙從吐出灰色血液的魔王手中飄落下來。
包括寫著【接觸到這張紙的人】／【會將這張紙誤認為《鏡像複寫紙》】的《一千零一頁的最後祈禱》和夾在裡面的《收納書》的二分之一書頁。

「你會以為那是我的王牌，是因為被竄改了記憶。」

我在自己腦中寫下的，用來欺騙魔王的「虛假記憶」啟動了。

〈豈有⋯⋯此理⋯⋯！妾身應已透過吸取血憶奪去汝之所有戰略⋯⋯！〉

確信自己獲得勝利，為了撕毀我的書頁而一瞬間解除「銀夜」的魔王被石化劇毒侵蝕著，憤恨地說道。
她已經無法再變回「銀夜」，閃躲我的攻擊了。

〈唔⋯⋯咿⋯⋯！不⋯⋯不過一旦引發變異性休克，汝將死⋯⋯！〉

「我才不會死。」

安排在《收納書》裡的一個《信封》從身體僵硬而無法動彈的魔王身邊飛到倒地的我頭上，然後打開。

「我說過了。我會賭上真祖給我的一切，打倒魔王。」

《鏡像複寫紙》
《一千零一頁的最後祈禱》

筆者〈阿爾緹莉亞〉為了讀者〈我〉，從讀者的記憶中排除的記憶〈書頁〉翩然飄落──

【接觸到這本《一千零一頁的最後祈禱》的瞬間，奧月綜嗣會回想起本來的戰略。】

「賭上我記憶〈生命〉的一切，我要救回阿爾緹莉亞！」

在我的記憶中寫下我所遺忘的記憶！

【你從變異性休克所引起的記憶回溯中復甦。】

啪嘰咿咿咿！大腦深處響起高亢的破碎聲，你從記憶回溯中甦醒了。

（⋯⋯是啊，我等這段描述很久了──《一千零一頁的最後祈禱》！）

取回刻劃在《書》之中的記憶，你從變異性休克中復甦了！
寫在這一頁的【我會抵抗變異性休克】的一行文字保護了你的心免於被吸血鬼化的浪潮侵襲！

「不⋯⋯可能⋯⋯！這⋯⋯這⋯⋯這和本魔王所安排好的劇本不同⋯⋯！」
「⋯⋯很遺憾，筆者。你以為自己透過吸取血憶得到的戰略、撕破的王牌，都是我寫進《最後祈禱》裡的虛假記憶！」

沒錯。讀者〈你〉所遺忘的你一直都在等待這個狀況，等待魔王確信自己獲勝，為了撕破你的書頁而解除「銀夜」的一瞬間。

「而現在，《鏡像複寫紙》──真祖阿爾緹莉亞為我重新裝填的護符就在我的手中！也就是足以擊敗魔王的故事劇本，用來搶奪《吸血奇譚》的武裝解除咒語！」

你捏緊真正的《複寫紙》，倚靠著鐘塔的殘骸站了起來。被石化劇毒侵蝕身體的魔王已經無法逃脫了！

（插圖p463）

「豈⋯⋯豈有此理！這⋯⋯『聖堂』的計劃不可能被破解！」

你奮力撐起差點引發變異性休克的身體，搖搖晃晃地離開瓦礫形成的牆壁。
為了在零距離之下擊發複寫在《複寫紙》上的破刃暴風槍。

「嗚⋯⋯別⋯⋯別過來！只要拖延至變異性休克發生，『聖堂』便確定獲勝！汝應已用盡一千頁之記憶容量！⋯⋯理⋯⋯理理⋯⋯理論障壁⋯⋯理論障壁呢⋯⋯！」
「我已經打碎魔王的理論障壁。脫逃手段也都排除了。我已經進入暴風槍的射程範圍。變異性休克的發生也會再延遲一頁。」

你對《最後祈禱》使用《再生紙》，使之成為全新的書頁。你在只存在一千頁的記憶容量中硬是創造了第一千零一頁。

「⋯⋯好了，作出了斷吧，魔王。」

這就是《一千零一頁的最後祈禱》真正的最後一頁。
你所剩下的記憶容量即將用盡。
現在就是改寫故事結局的，獨一無二的機會！

你掀開化成一塊破布的制服下擺，露出右手臂的肌膚。
為了從自己的血液中回收刻劃在奧月綜嗣這個人物之中的最後伏筆，「存在於記憶中卻又不存在之記憶」

「⋯⋯這部吸血奇譚的開始要回溯到五年前的十月十六日，父親遇害的那個慘劇之夜。我在那一天，在那個書齋見過吸血鬼真祖阿爾緹莉亞。」

這個故事的文章內還存在著另一個「過去刻劃在讀者心中的記憶」

被記憶回溯所封閉的絕望記憶──與五年前的十月十六日有關的記憶。

「可是我卻忘了阿爾緹莉亞。我原本以為那是因為我太過懦弱，無法承受絕望，才會把五年前的真相封印到記憶回溯的黑暗中。」

你握緊水晶護符，輕吸一口氣，然後低聲說道：

「──可是，那其實是真祖阿爾緹莉亞為了保護真相不被魔王侵害，才竄改的記憶。」

叮鈴。項鏈就像是要告訴你真相，在你的手中晃動。

「⋯⋯其實我一直很害怕知道真相。」

你無法接受而拋棄的記憶，現在也還在血管中循環著。裡面蘊含著能取回所有真相，從黑暗中救出真祖的最後記憶。

「⋯⋯五年前，父親被殺，只剩我活了下來，其中一定有著什麼意義。我一直覺得為了成就那個意義，回報那個意義⋯⋯我必須不斷地痛苦下去。」

你為了面對自己的懦弱，把自己一直以來視而不見的絕望轉換成言語。

「⋯⋯所以我逃避了。我想要忘記痛苦的事，想要改寫絕望，因而依賴《竄改記憶之書》。」

你必須戰鬥。你必須獲勝。
對手正是你想要遺忘的記憶，你自身的故事。

「可是現在我能夠相信。」

真祖灌注在護符裡的魔力就像是在呼應你的意念，變得更加明亮。

「我不會再逃避了。」

你祈禱般地握著護符，高潔地舉起右手臂。

「不管是絕望、傷痛、真相，還是愛情⋯⋯」

你用犬齒咬穿自己的肌膚，吸光涌出的血液與血憶。

「都是阿爾緹莉亞給我的東西。」

然後，我得知了五年前的真相。

借由自我吸血涌入的記憶就像走馬燈一樣流過我的眼前。
血液澆灌在過去的我身上。內臟和消化物從父親的身體中脫落。
肺髒從內側被加壓而破裂。腹腔和骨盆被倒下的提燈照亮。
頭部被擠壓得像鬼臉一般。眼球從眼窩中彈落到地面上。
持續折磨我五年的記憶回溯告訴我這些真相。

『⋯⋯聽好了，綜嗣⋯⋯我現在必須封印你的記憶。』

五年前，我失去記憶的理由。

『要擊敗魔王的「夜」，至少需要五年以上咏唱的魔素。「聖堂」恐怕不會給我這麼長的咏唱時間⋯⋯所以綜嗣，你必須離開圖書館都市五年，在日本等待時機來臨。』

五年前，我不得不離開圖書館都市的理由。

『咏唱中的魔力回路無法用來咏唱別的魔法。因此你開始咏唱時，我的咒縛魔法會奪走你的意識⋯⋯儘管痛苦，希望你能明白。』

五年前，我失去魔法的理由。

『⋯⋯為了保護伏筆，爸爸必須死⋯⋯遺言就托付給五年後的你。』

五年前，父親托付給未來的我的話。

『⋯⋯只是打開魔導書，只是深吸一口氣，只是依循著咒紋時；咒語只會是單純的咒語，而魔法也能是單純的魔法。』

五年前，偉大博士在將死之時刻劃在我心中的──

『⋯⋯但當你心裡掛念著某個重要的人，魔法就能超越魔法。交疊的呼吸會化為心跳，連結的咒紋會化為誓約，咏唱的咒語會化為祈禱；使魔法帶來奇跡。』

成為偉大博士的方法。

「啟動碼Lg100b──重新啟動咏唱。」

而我超越五年的時光，重新啟動取回真祖阿爾緹莉亞的咏唱。
為了用這五年來持續折磨我的絕望，記憶回溯所保護的五年前的魔法──

「什⋯⋯奧月綜嗣！難道汝一直誤以為自己無法使用魔法⋯⋯！」
「接招吧，魔王──我就用一頁的篇幅解決你。」

在這個戀愛故事的最後一刻，從記憶竄改中拯救阿爾緹莉亞。

「唔⋯⋯朱染七天，來者攜炎！」
「其乃撐天梁柱，鎮國礎石！」

兩道咏唱響徹銀白之夜，撼動空氣。

「破壞與再生之證，淨化之炎可毀天！」
「鎮守龍田之風暴女神啊！」

我的全身都散發著蒼藍色的光輝。依靠靈魂存在的魔力回路因過大的負荷而哀號著。因為想要貫穿魔王的理論裝甲，就需要對魔力回路持續灌注五年的龐大魔素，其魔力遠遠超越人類極限。
可是只要是為了阿爾緹莉亞，我就能超越我自己。

「宣告末日寒冬來臨之灼熱！發行──」
「充滿大氣之神靈且聽召喚！」

魔王的咏唱完成了。我手上已經沒有能夠阻止發行的魔導書。
可是我贏得了。因為阿爾緹莉亞給我的一切能夠保護我不受魔王傷害。

（⋯⋯真祖〈阿爾緹莉亞〉。）

「驅獄炎之天空王！」

（你之所以重新裝填這個護符──就是為了抵擋魔王現在的魔法吧。）

就像阿爾緹莉亞重新裝填的這個護符的理論障壁！
一瞬間，我的視野被白色填滿。
隨著砰的一聲，我的鼓膜破裂，聲音從世界上消失。
我感覺得到強大的輻射熱滲透了理論障壁，燒灼著我的外套和皮膚。

「⋯⋯以無形⋯⋯之臂──」

即使如此，我仍然沒有停止咏唱。
因為如果我無法阻止魔王，真祖一定會消失。
她會永遠忘了我，變回最邪惡的吸血鬼。

「束縛阻風之刃。」

只要能阻止這種事發生，我不管犧牲什麼都無所謂。
不管是失去魔法，被奪走記憶，失去夢想，被絕望折磨的五年人生。
還是此刻被變異性休克吞噬，即將消失的我的自我。
想要改寫的傷痕、痛苦和真相──我都可以為阿爾緹莉亞超越。

「此刻，為獻予她之祈禱與犧牲，賜以神代風暴之恩寵。」

因為我從五年前開始就一直喜歡著阿爾緹莉亞──

現在也仍然愛著阿爾緹莉亞。

交疊的呼吸化為心跳，連結的咒紋化為誓約，我將咒語和祈禱獻給真祖。

「發行──」

這五年來都留在魔力回路裡，持續吸收魔素的我的魔法啊。
請在這一千零一頁祈禱的最後──

引發奇跡。

「──破刃暴風槍！」

我在咒語裡寄托願望，解放魔力回路。
釋放出來的大量魔素燒毀神經系統，流竄而出。全身從內側沸騰，破裂的脊髓噴射出紅煙，將我往前推進。
帶著雷光的紅風捲起一陣神代風暴，形成漩渦──

以我持續灌注五年的魔素貫穿阿爾緹莉亞的障壁。

「啊啊啊啊啊啊啊！」

暴風之槍的尖端瘋狂肆虐，貫穿銀白之夜──

應聲破壞了魔王的魔法。

「嘎⋯⋯啊啊啊啊啊啊啊啊！」

慘叫震撼了黑暗。
對魔王阿爾緹莉亞來說，這肯定是臨死的最後吶喊。

「要決定結局的人，是我。」

我接住被武裝解除魔法彈飛的《吸血奇譚》，靜靜地宣言。

「呵⋯⋯哈⋯⋯哈哈⋯⋯哈哈哈哈哈⋯⋯本魔王竟也有毀滅的一天吶。」

阿爾緹莉亞的四肢五體被紅色風暴扭住，固定在空中。
雖然吸血鬼的不死之身讓她勉強存活⋯⋯但只要放著不管，魔王就會被父親的詛咒奪走吸取血憶得來的記憶和再生能力，然後毀滅。

「咕嘻⋯⋯哈哈⋯⋯啊嘰嘻嘻嘻。頭疼呀⋯⋯！妾身⋯⋯究竟⋯⋯被魔王⋯⋯非也⋯⋯將綜嗣⋯⋯殺死⋯⋯愛⋯⋯啊⋯⋯《吸血奇譚》⋯⋯啊哈哈哈哈哈哈！」

《吸血奇譚》被奪走，受到遺忘詛咒的侵犯，透過血憶吸取能力從我這裡流過去的真祖記憶混入腦中，讓魔王喃喃念著一些意義不明的話。
面對混合真祖與魔王的記憶，就要遺忘一切而死的吸血鬼──

「⋯⋯阿爾緹莉亞。」

我用愛怜的聲音呼喚她的名字。

「⋯⋯我想起來了。我從封印在記憶回溯中的記憶裡找回全部的真相了。還有以前的你和我寫下的，這個戀愛故事的伏筆。」

沒錯，這個故事是從頭到尾都巧妙安排的一連串伏筆。

「在《一千零一頁的最後祈禱》的序章，你明明是個吸血鬼，卻莫名地救了身為人類的我。你說理由是你曾經被我『救過』⋯⋯但我救了你的事並不是發生在兩天前，而是五年前對吧。」

十幾年前吸血鬼戰役結束後，被「聖堂」關押的魔王阿爾緹莉亞度過了漫長的囚犯生活，然後在五年前從「聖堂」的牢獄逃到迷宮中。
並且身負不吸食他人的血就會喪命的瀕死重傷。

「⋯⋯五年前，我在圖書迷宮的一個角落遇見被吊起、火燒過而筋疲力竭的一隻受傷吸血鬼。我把血分給那隻吸血鬼，救了瀕死的生命。」
「⋯⋯啊哈。」
「吸血鬼的記憶被詛咒了。知道她的記憶只能維持僅僅三分半的我，從父親的書齋中把《圖書迷宮的吸血奇譚》帶了出來。」
「嘻哈⋯⋯哈哈哈。」
「⋯⋯知道我救了吸血鬼，父親原本打算殺了她。我拚命求情，得到了『為《吸血奇譚》加上嚴密的防護機制』的條件。而那項機制完成的預定日就是五年前的十月十六日⋯⋯我的第十個生日。」
「啊哈⋯⋯啊哈哈哈哈哈哈。」
「⋯⋯《圖書迷宮的吸血奇譚》原本是我和吸血鬼的生日禮物。圖書迷宮的銀夜原本會在那個十月十六日脫胎換骨，成為我的朋友。」
「啊哈哈⋯⋯啊哈哈哈哈哈！」
「──直到被『聖堂』竄改記憶的奧月綜嗣〈我〉在《吸血奇譚》上寫下命令為止。」

換句話說，筆者就是我。
父親死亡、阿爾緹莉亞行凶、我喪失魔法的原因，都在於我奧月綜嗣。
要從真祖或偉大博士手中奪取魔導書是很困難的，而且失敗時的風險難以估計。所以「聖堂」綁架了我，在我的記憶中寫下了致命的陷阱。

「我想起來了。我從父親手中接過《圖書迷宮的吸血奇譚》時，我的確親手寫下了【你要殺死奧月戒】⋯⋯然後悲劇就發生了。」

也就是說，我被當成了魔王這個生物兵器的遙控器。
只要竄改我的記憶，讓我使用啟動碼竄改《圖書迷宮的吸血奇譚》，就能在不接觸危險吸血鬼的情況下，將她當作生物兵器來使用。
這就代表我們的命運從五年前開始就一直被筆者改寫了。

「我找到答案了。」

五年前的真相。

「阿爾緹莉亞，你不是我的殺父仇人。」

殺了父親的人是魔王，而策劃了這一切的是操控了我的「聖堂」

「啊⋯⋯啊哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈！」
「⋯⋯所以你不必再笑了，不必再假裝瘋狂。我知道那都是騙人的。」

我緊握《圖書迷宮的吸血奇譚》，帶著祈願下達命令：

「啟動碼Rz999f──【破壞竄改過的記憶，《圖書迷宮的吸血奇譚》】！」

清亮的聲音響起，《書》散發光輝。
獨自打開的書頁中溢出無數文字，融進「夜」裡，消失不見。

「哈哈哈哈哈⋯⋯啊哈哈⋯⋯哈哈⋯⋯哈。」

魔王的笑聲停止了。
最強吸血鬼，能力吞噬者阿爾緹莉亞──

「⋯⋯為何？」

那對清澈的深紅色眼睛流出悲傷的淚水。

「為何⋯⋯為何不殺了妾身？若不殺死妾身⋯⋯若不奪去《圖書迷宮的吸血奇譚》，汝就要遭到變異性休克吞噬，發狂而死了吶！」

那是阿爾緹莉亞這五年來一直隱藏的感情激流。

「為何⋯⋯！為何不在妾身還是個惡人的情況下殺死妾身！為何要讓妾身想起不願想起的記憶！若是直接殺死妾身，妾身就不必如此悲傷了！」

阿爾緹莉亞慟哭。她認定自己是黑暗魔王，是人類的天敵。
擅自斷定自己從我的身邊消失才能讓我幸福。

「五年⋯⋯！妾身可是等了整整五年呀！在沒有汝的迷宮裡只身一人！妾身渴望與汝再會，獨自度過了五年吶⋯⋯！結果呢？竟僅能共度三日？別鬧了，妾身這份情意該如何是好呀！」

傾吐著壓抑了五年的悲戀，阿爾緹莉亞又哭又叫。

「若妾身是汝之殺父仇人，妾身還能忍！若能讓汝繼續前進，妾身甘願被殺！然而汝卻說妾身並非仇人？有權與汝共同存活？⋯⋯妾身不依！不依不依不依！妾身不願死，不願死得戀情無疾而終！」

吸血鬼明明超越了生死──

卻因恐懼死亡而吶喊。

「⋯⋯五年前，你也這麼說過。」

在這個圖書迷宮的黑暗深處，我遇見了失去記憶的吸血鬼。
吸血鬼在通往死亡的陷阱中流淚喊著不想死，而我對她發誓。

「所以我許下願望，希望成為能拯救你的人。」

成為能拯救阿爾緹莉亞的人──成為魔法師。

「⋯⋯我會發誓成為偉大博士，不是因為父親被殺。」

我打開《吸血奇譚》的封面，翻到第一頁。
寫在上面的一行文字，是五年前的我許下的願望：

【我想要變成救得了阿爾的人。】──1001pgs。

「喜歡上你，就是這個故事的開頭。」

沒錯。我希望自己能「拯救他人」，不是因為父親被殺。
從我在圖書迷宮的黑暗深處找到受傷吸血鬼的那一天開始──

我和阿爾緹莉亞的故事就編織出了《圖書迷宮的吸血奇譚》

「我對《圖書迷宮的吸血奇譚》祈求了【能從黑暗中拯救魔王的魔法】。」

而《圖書迷宮的吸血奇譚》回應了我的祈禱。
它在我心中植入心理創傷，封印我的魔力回路，讓我這五年來都無法使用魔法。為了讓奧月綜嗣持續裝填足以貫穿魔王的理論障壁的大量魔素。
它給我考驗和絕望，使我五年來都痛苦得能夠堅持自己的信念。讓我就算被「聖堂」扭曲故事，也不會放棄阿爾緹莉亞。

「然後跨越了五年的時光，《圖書迷宮的吸血奇譚》把我引導到了真祖〈你〉的面前。」

──《圖書迷宮的吸血奇譚》是個隨處可見的故事。

一個男孩喜歡上一個女孩。
男孩想要幫助有困難的女孩。

《圖書迷宮的吸血奇譚》就只是這樣的一個故事。

「只為了這一個願望，《圖書迷宮的吸血奇譚》寫出了奧月綜嗣〈主角〉這個人物。」

為了從深深的黑暗中救出我愛上的吸血鬼。
為了從覬覦吸血鬼真祖之力的「聖堂」手中保護阿爾緹莉亞。
以名為父親之死的絕望。以名為喪失魔法的考驗。
以名為艾莉卡的敵對角色。以名為卡露米雅的連結過去之關鍵。
以名為魔王背叛的失戀。以名為虛構世界的急轉直下。

《圖書迷宮的吸血奇譚》把奧月綜嗣引導到了漫長故事的結局。
為了在這個瞬間、這個地方、這個戀愛故事的最後──

「阿爾，我來救你了。」

實現我的願望。

超越五年的時光，《圖書迷宮的吸血奇譚》終於迎向結局。
那個時候沒能傳達的心意，被「聖堂」塗改的戀愛──

我終於能寫進這個故事裡了。

「我愛你，阿爾緹莉亞。」

寫下這個戀愛故事的終章。

「嗚⋯⋯啊⋯⋯啊啊⋯⋯啊⋯⋯」

仿彿盛裝著鮮血的雙眼落下透明的淚珠。
阿爾緹莉亞很清楚。
我的記憶存量用盡的現在，這部戀愛故事就永遠不會再有續集。

《圖書迷宮的吸血奇譚》就快要結束了。

「⋯⋯嗚啊啊⋯⋯嗚啊啊啊啊啊啊啊啊啊啊啊啊！」

吸血鬼仰天哭喊。
那並非可怜自身境遇的眼淚，而是哀悼吸血奇譚完結的痛惜之淚吧。

「⋯⋯阿爾緹莉亞，在我忘記一切之前，希望你告訴我一件事。」

我能明白。阿爾緹莉亞一定也感覺到了。
在短短的剎那之間，奧月綜嗣的記憶就會被瘋狂吞噬，然後消滅。

「⋯⋯我喜歡你。我愛你勝過世界上的任何一個人。」

所以我必須發問。我必須問出這個真相。

「所以拜託你，阿爾緹莉亞。」

我必須為這段愛的告白尋求答案。

「──請說你喜歡我。」
「⋯⋯嗚⋯⋯啊。」

被囚禁在遺忘牢獄中的吸血鬼有兩只，能解除詛咒的《書》卻只有一本。
除非放棄其中之一，否則兩者都無法存活。

「回答我吧，阿爾緹莉亞。在死亡讓我們永別之前。」

我們不得不抉擇。
選出我們之中的誰能存活，誰必須死。

「汝⋯⋯汝這傻子實在壞心⋯⋯！」

我一步一步靠近哽咽著大叫的阿爾緹莉亞。

「是呀沒錯，妾身就是愛慕著汝！不過稍微受了點溫情，竟就這麼患上五年的相思病，妾身也自覺過於痴情吶！」

我跨越鐘塔的殘骸和血海，為了完成過去的誓約。

「即使如此⋯⋯即使如此妾身也感到歡喜！妾身滿心想著對人類的復仇，在黑暗中徘徊，瀕臨死亡──汝卻對妾身說『吸我的血吧』！只有敵人與孤獨的妾身在汝身邊初嘗友情與愛情⋯⋯！

於是妾身無可救藥地為汝傾心呀！何錯之有！」

「這怎麼可能是錯的。」

我用指尖輕輕撈起正要滑落臉頰的淚水。
我順勢抬起她的下巴──

「因為我終於能完成五年前的誓言了。」

我奪去阿爾緹莉亞的雙唇。

這一切都是從五年前開始的伏筆。
奪走對手能力的能力、經由體液吸取血憶、我保留著人性的事、兩個能力吞噬者、能勝過魔王的王牌，全部都是戀愛故事的伏筆。
喪失人性的我透過記憶竄改獲得真祖等級的血憶吸取能力。
阿爾緹莉亞身負瀕死的重傷，血憶吸取能力已經衰弱到與我無異。
因此名為接吻的體液交換會持續讓我的人性與血憶吸取能力加倍到極限──

生與人性和記憶會給予阿爾緹莉亞。
死與吸血鬼性和遺忘則由我奪走。
將一分為二，我就能從遺忘的牢獄中救出阿爾緹莉亞。

「⋯⋯噗呼！」

與最初也是最後的吻一起，《圖書迷宮的吸血奇譚》迎向結局。
為了【救阿爾】，我奪去了所有的殘酷。
不管是逼近的黑暗、流入的記憶，還是排斥反應引起的死亡──

全都由我吸取，然後死去。

「⋯⋯永別了，阿爾緹莉亞。」

於是──

我的故事結束了。

【限時記憶開始解凍。】

喀鏘。

在走向死亡的思緒深淵，就要完結的故事最深處，齒輪的聲音輕輕響起。

『⋯⋯噗嗤，竟說「我的故事結束了」呢！汝果然是個傻子吶♪』
「咦？」
『嗯呵呵，早安吶，汝♪歡迎再次來到筆者的領域♫解讀出安排於故事中之伏筆，查明五年前的真相，取回記憶與魔法並救出妾身的感受如何呀？就這麼赴死豈不可惜？』
「等⋯⋯等一下！我不是死了嗎？我吸走了真祖的遺忘詛咒，把你變成人類，代替你死了吧！為什麼我還有意識啊！」
『此乃後記結尾。』
「後記結尾？」
『這個嘛，主角〈汝〉一死，可就無法再寫續集，發展為一個系列了吶。況且若還未收回最後的伏筆，就因主角之死而完結，豈不是無趣至極？』
「咦⋯⋯最⋯⋯最後的伏筆是指⋯⋯？」
『沒錯，汝還未解決安排於此故事中之最大且最後的伏筆⋯⋯』

【寫出這個故事的真正筆者究竟是誰？】

「！」

你對響起的聲音抬起頭的瞬間，墨水般的黑點凝結起來，在空中寫出文字。
你直覺地理解到，這裡是故事的背後，幻想的外部。
是執筆這部小說的筆者的領域。

「咦⋯⋯呃，抱歉，我有點搞不清楚狀況⋯⋯？」

⋯⋯唉。你難道不曾覺得這個故事「一切都太剛好了」嗎？

你認為對「聖堂」來說是魔王的遙控器的你是怎麼逃出亞歷山卓的？殺死令尊的那幫人有可能允許這種事發生嗎？

「這⋯⋯這個嘛⋯⋯呃⋯⋯」

追根究底，你回到亞歷山卓的第一天就遇到真祖阿爾緹莉亞和殺人魔艾莉卡也很奇怪吧。三個人偶然在廣大的圖書迷宮中相遇的機率幾乎趨近於零。
除此之外還有其他種種命運般的偶然，才讓你得以拯救阿爾緹莉亞。

「咦⋯⋯難⋯⋯難道說那些偶然都是筆者安排的嗎！」

是的。

筆者操縱真祖的身體，甩開「聖堂」的追捕，讓你逃往國外。
筆者封印了你的記憶，讓你持續儲存魔素到魔力回路裡。
筆者帶給你絕望，鍛鍊你的精神，使你能夠克服困難。
筆者跨越五年的時光，將你喚回圖書館都市。
筆者吸引你到圖書迷宮，讓你與吸血鬼真祖重逢。
筆者引導艾莉卡，讓她巧遇並殺死你。
筆者引誘卡露米雅採取行動，讓過去的魔王吸取她的血液和記憶。
筆者讓你聆聽魔法理論的課程，筆者給你肉搏戰的技能，筆者讓你注意到透過唾液能吸取血憶，筆者讓你使用咒血能力，筆者促成了魔法的復活。

這部《吸血奇譚》的真正筆者，才是描寫了你的一切的人。

《圖書迷宮的吸血奇譚》絕對不只是一部描寫一連串偶然的小說。一切都是筆者所安排的必然，為了抵達這個結局和開頭的伏筆。
寫出這整部故事的，《圖書迷宮的吸血奇譚》的真正筆者就是──

【如果有奇跡發生，希望可以永遠永遠和綜嗣在一起。】

『就是希望與汝共同活下去的，五年前的阿爾緹莉亞。』

「⋯⋯阿爾⋯⋯緹莉亞⋯⋯」
『⋯⋯汝，妾身在這部《吸血奇譚》的序章，許下了一個奇跡。』

阿爾緹莉亞靦腆地笑了，對你說出自己的秘密願望：

『倘若因命運而分別的妾身與汝總有一天能跨越時間與距離，再次於圖書館都市重逢，再次墜入愛河。』

在《吸血奇譚》的第一頁，那句話就寫在你的願望旁邊。

『倘若得知妾身過去的汝仍然願意愛著妾身。』

那是安排在故事裡的最大伏筆。

『但願於此戀愛故事之結局⋯⋯』

被記憶竄改囚禁的真實願望。

『妾身能與汝一生相伴。』

愛和奇跡的童話故事的──

一開始的第一頁。

『⋯⋯綜嗣，本吸血鬼真祖共享血液與血憶的，妾身最愛的眷屬⋯⋯妾身愛慕著汝。妾身愛汝勝過這世上的任何一人。』

在這個名為《圖書迷宮的吸血奇譚》的故事，深愛著人類的吸血鬼的最後祈禱──

『因此，妾身此刻還望汝回應。』

阿爾緹莉亞尋求你的答案。

『即使失去記憶，汝可願再次對妾身傾訴愛意？』

「⋯⋯當然願意了，笨蛋吸血鬼。」

你傻眼地輕聲笑道，注視著阿爾緹莉亞的鮮紅眼眸。
然後你對吸血鬼真祖，對這部《圖書迷宮的吸血奇譚》回應誓約之言：

「我答應你，阿爾緹莉亞。不管失去記憶幾次，我都一定會再度愛上你。」

喀鏘。

宣告開始的齒輪之音在筆者的領域裡高亢地響起。

【啟動碼認證完畢。開始執筆「奇跡」。】

五年前的十月十六日，你和阿爾緹莉亞相遇的奇跡──

是通往這部《圖書迷宮的吸血奇譚〈故事〉》序章的，戀愛故事的最大伏筆。

【借由你所給予的血液，阿爾緹莉亞取回了記憶，成為一名人類。】

突如其來的啟動咒語破壞了登場人物死亡的缺憾結局──

將故事竄改為皆大歡喜的快樂結局。

【你失去了所有被竄改過的記憶，從變異性休克中恢復。】

於是，《圖書迷宮的吸血奇譚》化為一個奇跡。
通往誰也沒有想像過的，戀愛故事的序章。


◆　　◆　　◆　　◆　　◆　　◆　　◆　　◆　　◆　　◆　　◆　　◆

讀完到目前為止的故事，你從虛構小說中覺醒。

「⋯⋯呼，終於看完了。」

下午的教室景象映照在緩緩睜開的眼裡。同學互相交談的吵雜聲響充滿四周，靜靜地回到原本被《書》支配的聽覺中。

「⋯⋯《圖書迷宮的吸血奇譚》。《竄改記憶與世界的書》啊。」

直到剛才為止還存在於眼前的小說世界已經消失得無影無踪。
筆者的領域轉換為「藥草院」的教室，【文字】已經不見踪影，與魔王戰鬥的激動情緒也被窗邊吹來的風靜靜地奪走。
你打開一本《書》，單手撐著臉，坐在教室窗邊的書桌前。

「⋯⋯那起事件到現在已經過了一個星期啊⋯⋯真是的，明明被捲入『聖堂』的計謀和人類滅亡的危機，我卻什麼都不記得了。」

從變異性休克中覺醒時，你就已經失去剛抵達亞歷山卓的那三天的所有記憶。
為了拯救你免於變異性休克的侵害，就必須除去從外部寫入的記憶，也就是寫入《一千零一頁的最後祈禱》的資訊。
其中包括關於艾莉卡和同學的記憶，圖書館都市的記憶，破壞「聖堂」的計劃的記憶。
以及與阿爾緹莉亞的戀愛。

「⋯⋯阿爾緹莉亞。」

你低聲呼喚從記憶中消失的愛人之名。
班會前的熱鬧教室裡沒有吸血鬼真祖的身影。你的制服連衣帽裡也沒有藏著折好的連身裙和銀白色的蝙蝠。
就像卡露米雅在《一千零一頁的最後祈禱》裡曾說過的，吸血鬼會被處以死刑，所以你果然是不可能和迷宮最強的吸血種一起念書的。
如果沒有用接收自阿爾緹莉亞的《圖書迷宮的吸血奇譚》來竄改吸血鬼性的話，繼承了最強吸血鬼之力的你應該也逃不過處刑的命運。

「⋯⋯阿爾緹莉亞給我的東西還在保護著我啊。」

你觸碰胸前的護符，回憶起吸血鬼真祖。
日子平安地一天一天過去。流逝的時光將燒毀的「圖書館」、崩塌的升降機、損壞的鐘塔都遺忘。
就連「聖堂」的陰謀和魔王復活的危機都像是沒有發生過似的。
看著你和阿爾緹莉亞取回的，渺小又隨處可見的溫暖日常──即使如此⋯⋯

你還是有一種預感。

「⋯⋯哎呀，奧月同學。你看起來很開心呢。」
「嗯？是嗎？你看起來也很高興耶。」

聽見呼喚自己的聲音，你抬起頭，發現艾莉卡那雙深青色的眼睛正在對你微笑。她在《書》中的戰鬥受到的燒傷似乎已經完全痊癒了。

「呵呵，今天的班會延遲的理由，我已經聽卡露米雅同學說過了。」
「喵呵呵！大家回到位子上！我帶小葉老師來了喵！」
「老師被帶來嘍！各位同學請回座，班會要開始了！」
「⋯⋯呵呵，才剛提到，人就來了呢。」

那是個預感。

「等一下會跟大家說明，因為有很多事要準備，抱歉來晚了。最近老師真的是忙得不可開交⋯⋯」
「啊，小葉老師，這邊看得到你的肩膀有貼布喵。」
「這邊嗎？不不不能看啦，卡露米雅同學不可以看！」
「⋯⋯小⋯⋯小葉老師，已經過了班會開始的時間⋯⋯」
「啊，對喔！那⋯⋯那麼各位同學，現在班會時間開始！」

《一千零一頁的最後祈禱》的頁數用盡，也什麼都不會結束的預感。

《圖書迷宮的吸血奇譚》一打開，書上一定寫著奇跡的預感。

那一定是故事開始的預感。

「那麼要開始嘍。阿爾同學！請進！」
「失⋯⋯失禮了吶！」

這個聲音響起的同時，教室前方的門砰的一聲打開。
拖著一頭銀白色長髮的少女走進教室，用生硬的步伐登上講台。

「⋯⋯呃⋯⋯呃，該說什麼才好呢⋯⋯啊！喏⋯⋯喏，汝還不快幫幫妾身！妾身不擅應付這般場面吶！」

少女的鮮紅雙眼游移著，一發現你的身影便向你求助。
你抱著逐漸高漲的預感，露出苦笑回答原本是吸血鬼的友人：

「真是的，不是練習過了嗎？首先要自我介紹和打招呼啦。」
「啊，確⋯⋯確實如此！好⋯⋯好！」

少女用紅潤的雙頰做出最燦爛的笑容，對同學高聲說出自己的名字：

「妾身名喚阿爾緹莉亞！雖不諳人類習慣，還望諸位關照！」

好了，戀愛故事即將開始。
主角是曾為人類的吸血鬼，女主角是曾為吸血鬼的女孩。
配角是身為混血惡魔的同學、開朗的貓耳班長、外表年齡十歲的博士。
情節有些陳腐，角色也還相當不足。
不過相愛的人類和吸血鬼的幸福會永遠持續下去。
往後的故事也一定會變得愈來愈有趣。
所以直到頁數用盡之前，請繼續撰寫這個故事。

【距離喪失記憶　還剩∞頁】

撰寫由你編織，由我記錄的──《圖書迷宮的吸血奇譚》